import java.util.Scanner;

public class UserRegister {
    public UserRegister(String user){
        Scanner sc = new Scanner(System.in);
        System.out.println("\nEnter Username");
        String username = sc.next();
        String password = "";
        int i = 3;
        PasswordEncryption obj = new PasswordEncryption();
        while(i != 0){
            System.out.println("\nEnter Password : ");
            password = sc.next();
            if(password.length() < 5 || password.length() > 8){
                i--;
                System.out.println("\nPassword length must be between (5-8)\nNo of Attempts remaining : " + i);
            }else{
                break;
            }
        }
        if(i == 0){
            System.out.println("\nTry again");
            System.out.println("-----------------------------------------------------------------------");

        }else {
            if (user.equals("Admin")) {
                UserLogin.userAdmin.put(username, obj.encrypt(password));

            } else if (user.equals("Vendor")) {
                UserLogin.userVendor.put(username, obj.encrypt(password));

            } else {
                UserLogin.userCust.put(username, obj.encrypt(password));

            }
            System.out.println("\nRegistered successfully\n");
            System.out.println("-----------------------------------------------------------------------");
            System.out.println("\nLogin to continue\n");
        }
    }
    /*
    public Encryption getEncryption(){
        PasswordEncryption obj = new PasswordEncryption();
        return obj;
    }
    */
}

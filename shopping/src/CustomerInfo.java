public class CustomerInfo extends PersonInfo{
    private String shipAddr;
    private String billAddr;

    CustomerInfo(String name, String phno, String billAddr, String shipAddr){
        super(name,phno);
        this.billAddr = billAddr;
        this.shipAddr = shipAddr;
    }
    public String toString() {
        return  "\nCustomer name : " + super.getName() + "\nPhone no : " + super.getPhno()  +  "\nBilling Address : " + billAddr + "\nShipping Address : " + shipAddr;
    }
}

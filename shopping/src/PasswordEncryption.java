public class PasswordEncryption implements Encryption{

        public String encrypt(String plaintext) {
            StringBuilder encryptedText = new StringBuilder();

            for (char ch : plaintext.toCharArray()) {
                if (Character.isLetter(ch)) {
                    char base = Character.isUpperCase(ch) ? 'A' : 'a';
                    char encryptedChar = (char) (base + (ch - base + 3) % 26);
                    encryptedText.append(encryptedChar);
                } else if (Character.isDigit(ch)) {
                    char encryptedChar = (char) ('0' + (ch - '0' + 3) % 10);
                    encryptedText.append(encryptedChar);
                } else {
                    char encryptedChar = (char) ((ch + 3 - 32) % 95 + 32);
                    encryptedText.append(encryptedChar);
                }
            }

            return encryptedText.toString();
        }


        public String decrypt(String ciphertext) {
            StringBuilder decryptedText = new StringBuilder();

            for (char ch : ciphertext.toCharArray()) {
                if (Character.isLetter(ch)) {
                    char base = Character.isUpperCase(ch) ? 'A' : 'a';
                    char decryptedChar = (char) (base + (ch - base - 3 + 26) % 26);
                    decryptedText.append(decryptedChar);
                } else if (Character.isDigit(ch)) {
                    char decryptedChar = (char) ('0' + (ch - '0' - 3 + 10) % 10);
                    decryptedText.append(decryptedChar);
                } else {
                    // For symbols and other characters, simply shift their ASCII values
                    char decryptedChar = (char) ((ch - 3 - 32 + 95) % 95 + 32);
                    decryptedText.append(decryptedChar);
                }
            }

            return decryptedText.toString();

        }

}



import java.util.HashMap;
import java.util.Map;

public class OfferDetails {
    static Map<Integer, Integer> offerList;
    OfferDetails(){
        offerList = new HashMap<>(){{
            put(500, 5);
            put(1000, 10);
            put(2500, 15);
            put(5000, 20);
            put(10000, 30);

        }};
    }

    public float getDiscount(float amt){

        int percentage = 0;

        for(Map.Entry<Integer, Integer> me: offerList.entrySet()){
            if(me.getKey() <= amt){
                //System.out.println("check");
                percentage = Math.max(percentage, me.getValue());
            }
        }
        if(percentage == 0){
            System.out.println("\nNo offer applicable");
            return amt;
        }
        System.out.println("Congratulations!! You have received a discount of " + percentage + "%\n");
        float payAmount = amt - (((float)percentage/100) * amt);
        System.out.println("After discount : ");
        return payAmount;
    }

    public void addOffer(int amt, int percent){
        if (offerList.containsKey(amt)) {
            System.out.println("Amount already exists\n");
        }else{
            offerList.put(amt,percent);
            System.out.println("Added successfully\n");
        }
    }

    public void deleteOffer(int amt){

        if (offerList.containsKey(amt)) {
            offerList.remove(amt);
            System.out.println("Deleted successfully\n");
        }else{
            System.out.println("No such amount exists\n");
        }

    }

    public void viewOffer(){
        System.out.println("\nThe Offers available are : ");
        for(Map.Entry<Integer, Integer> me: offerList.entrySet()){
            System.out.println("\nAmount : " + me.getKey() + "   Discount Percentage : " + me.getValue());
            System.out.println();
        }
    }

}

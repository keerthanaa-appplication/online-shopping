import java.util.HashMap;
import java.util.Map;

public class CustomerDetails{
    public static Map<Integer, Object> customerList = new HashMap<>();

    public static void displayCustomerDetails(int cid){
        for(Map.Entry<Integer, Object> me: customerList.entrySet()){
            if(me.getKey() == cid){
                System.out.println("CID : " + me.getKey() + me.getValue());
                System.out.println();
            }
            else{System.out.println("\nNo Customer information available");}
        }
        System.out.println("----------------------------------------------------------------");

    }
}

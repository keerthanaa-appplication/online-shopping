public class CartActions{
    OfferDetails off = new OfferDetails();

    public void viewReview(int pid){
        if (ProductStore.productList.containsKey(pid)){
            Product p = (Product) ProductStore.productList.get(pid);
            if(p.getReview().isEmpty()){
                System.out.println("No Review or Rating available");
            }else{
                System.out.println("\nRating : " + p.getRating());
                System.out.println("\nReview : " + p.getReview());
            }
        }else{
            System.out.println("No such product exists");
        }

    }

    public void addReview(int pid, String review, int rating){
        if (ProductStore.productList.containsKey(pid)){
            Product p = (Product) ProductStore.productList.get(pid);
            p.setRating(rating);
            p.setReview(review);
            System.out.println("\nReview added successfully");

        }else{
            System.out.println("No such product exists");
        }
    }

    public float orderSummary() {
        float sumTotal = 0;
        System.out.println("The items purchased are : ");
        for (int value : Cart.cartList.keySet()) {
            int qt = Cart.cartList.get(value);
            Product p = (Product) ProductStore.productList.get(value);
            SoldProductDetails.soldProductList.put(value, qt);
            sumTotal = sumTotal + (p.getPrice() * qt);
            System.out.println("\nPID : " + value + p + "\nQuantity : " + qt);
        }
        System.out.println("\nYour total bill amount is " + sumTotal);
        sumTotal = off.getDiscount(sumTotal);
        return sumTotal;

    }

}

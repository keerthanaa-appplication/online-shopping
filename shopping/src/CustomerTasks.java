import java.util.Scanner;

public class CustomerTasks {
    public CustomerTasks(){
        int flag = 1;
        CartActions obj = new CartActions();
        Scanner sc = new Scanner(System.in);
        while(flag == 1) {
            System.out.println("-----------------------------------------------------------------------");
            System.out.println("CUSTOMER TASKS");
            System.out.println();
            System.out.println("Choose one\n");
            System.out.println("1. View Products\n");
            System.out.println("2. Search a product\n");
            System.out.println("3. Add to Cart\n");
            System.out.println("4. Display Cart\n");
            System.out.println("5. Remove from Cart\n");
            System.out.println("6. Add personal Info\n");
            System.out.println("7. Print Order Summary\n");
            System.out.println("8. View ratings and review of a Product\n");
            System.out.println("9. Provide ratings and review\n");
            System.out.println("10. Exit from Customer Tasks\n");
            System.out.println();
            System.out.print("enter : ");
            int ch = sc.nextInt();
            System.out.println();
            System.out.println("-----------------------------------------------------------------------");
            ProductActions p = new ProductActions();
            ProductListActions listObj = new ProductListActions();
            switch (ch) {
                case 1:     //View Products
                    listObj.displayProductQty(AdminTasks.adminProductList);
                    break;

                case 2 :    //search a product
                    System.out.println("\nSearch by name or PID\n\n1. PID\n\n2. Name\n");
                    int choice = sc.nextInt();
                    if(choice == 1){
                        System.out.println("\nEnter PID: ");
                        int pid3 = sc.nextInt();
                        p.searchProduct(VendorTasks.vendorProductList, pid3);
                    }
                    else{
                        System.out.println("Enter Product name: ");
                        String pname1 = sc.next();
                        p.searchProduct(VendorTasks.vendorProductList, pname1);
                    }
                    break;

                case 3:     //Add to Cart
                    listObj.displayProductQty(AdminTasks.adminProductList);
                    System.out.println("\nEnter the PID : ");
                    int pid = sc.nextInt();
                    System.out.println("\nEnter the Quantity : ");
                    int qty = sc.nextInt();
                    if(qty <= AdminTasks.adminProductList.get(pid)){
                        listObj.addProductQty(Cart.cartList, pid,qty);
                        System.out.println("--------------------------------------------------------------");
                        System.out.println("Product added successfully with PID: " + pid);
                        System.out.println("--------------------------------------------------------------");
                    } else if (AdminTasks.adminProductList.get(pid) == 0) {
                        System.out.println("\nProduct out of stock");
                    } else{
                        System.out.println("\nRequested quantity not available");
                    }

                    break;

                case 4:     //Display Cart
                    listObj.displayProductQty(Cart.cartList);
                    break;

                case 5:     //Remove from Cart
                    listObj.displayProductQty(Cart.cartList);
                    System.out.println("\nEnter the PID : ");
                    int pid2 = sc.nextInt();
                    System.out.println("\nEnter the Quantity : ");
                    int qty2 = sc.nextInt();
                    listObj.deleteProductQty(Cart.cartList, pid2,qty2);
                    break;

                case 6:     //Add personal Info
                    String phno = "";
                    int i = 3;
                    System.out.println("\nEnter Name : ");
                    String name = sc.next();
                    while(i != 0){
                        System.out.println("\nEnter Phone number : ");
                        phno = sc.next();
                        if(phno.length() != 10){
                            i--;
                            System.out.println("\nNot a valid phone number\nNo of Attempts remaining : " + i);
                        }else{
                            break;
                        }
                    }
                    if(i == 0){
                        System.out.println("\nTry again");
                        break;
                    }else{
                        sc.nextLine();
                        System.out.println("\nEnter Billing Address : ");
                        String billAddr = sc.nextLine();
                        sc.nextLine();
                        System.out.println("\nEnter Shipping Address : ");
                        String shipAddr = sc.nextLine();
                        CustomerInfo cust = new CustomerInfo(name, phno, billAddr, shipAddr);
                        CustomerDetails.customerList.put(UserLogin.cid,cust);
                        CustomerDetails.displayCustomerDetails(UserLogin.cid);
                        break;
                    }

                case 7 :    //Print Order Summary
                    if (Cart.cartList.isEmpty()) {
                        System.out.println("\nNo products in the cart");
                    }
                    else{
                        if(CustomerDetails.customerList.containsKey(UserLogin.cid)){
                            System.out.println("\nConfirm to place your order (yes/no) : ");
                            String ans = sc.next();
                            if(ans.equals("yes")){
                                System.out.println("\nCustomer details : ");
                                CustomerDetails.displayCustomerDetails(UserLogin.cid);
                                float bill = obj.orderSummary();
                                System.out.println("\n\nTOTAL BILL = " + bill);
                                System.out.println("\n\n*****WELCOME*****");
                            }else{
                                System.out.println("\nOrder not placed");
                            }
                        }else{
                            System.out.println("\nAdd personal information to place an order");
                        }
                    }
                    break;

                case 8 :    //View ratings and review of a Product
                    System.out.println("Enter PID : ");
                    int pid3 = sc.nextInt();
                    obj.viewReview(pid3);
                    break;

                case 9 :    //Provide ratings and review
                    System.out.println("Enter PID : ");
                    int pid4 = sc.nextInt();
                    if(Cart.cartList.containsKey(pid4)){
                        sc.nextLine();
                        System.out.println("Provide review : ");
                        String review = sc.nextLine();
                        System.out.println("Provide rating (1-5) : ");
                        int rating = sc.nextInt();
                        obj.addReview(pid4, review, rating);
                    }else{
                        System.out.println("\nYou are not eligible to add review for that product");
                    }
                    break;

                case 10 :
                     flag = 0;
                     break;
            }
        }
    }
}

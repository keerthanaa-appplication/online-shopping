import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class VendorTasks {
    static int pid = 0;
    static boolean check = true;
    public static Map<Integer, Integer> vendorProductList = new HashMap<>();
    public VendorTasks() {
        ProductListActions listObj = new ProductListActions();
        //Initially available products
        ProductActions obj = new ProductActions();
        if(check){
            obj.addProduct(++pid, "shoe", 3000, "puma");
            listObj.addProductQty(vendorProductList, pid, 300);
            obj.addProduct(++pid, "shirt", 1200, "polo");
            listObj.addProductQty(vendorProductList, pid, 600);
            obj.addProduct(++pid, "pant", 1500, "vanHeusen");
            listObj.addProductQty(vendorProductList, pid, 600);
            obj.addProduct(++pid, "belt", 500, "black");
            listObj.addProductQty(vendorProductList, pid, 100);
            obj.addProduct(++pid, "cap", 70, "black");
            listObj.addProductQty(vendorProductList, pid, 50);
            obj.addProduct(++pid, "socks", 300, "nike");
            listObj.addProductQty(vendorProductList, pid, 200);
            obj.addProduct(++pid, "wallet", 450, "indigo");
            listObj.addProductQty(vendorProductList, pid, 100);
            obj.addProduct(++pid, "salwar", 2000, "trends");
            listObj.addProductQty(vendorProductList, pid, 500);
            obj.addProduct(++pid, "saree", 1600, "silk");
            listObj.addProductQty(vendorProductList, pid, 500);
            obj.addProduct(++pid, "bag", 2300, "zudio");
            listObj.addProductQty(vendorProductList, pid, 250);

            check = false;
        }

        Scanner sc = new Scanner(System.in);
        int flag = 1;
        while (flag == 1) {
            System.out.println("-----------------------------------------------------------------------");
            System.out.println("VENDOR TASKS");
            System.out.println();
            System.out.println("choose one\n");
            System.out.println("1. Add a Product in Vendor List\n");
            System.out.println("2. Edit a Product in Vendor List\n");
            System.out.println("3. Delete a Product in Vendor List\n");
            System.out.println("4. Display Products in Vendor List\n");
            System.out.println("5. Search a Product in Vendor List\n");
            System.out.println("6. Exit from Vendor Tasks\n");
            System.out.println();
            System.out.print("enter : ");
            int ch = sc.nextInt();
            System.out.println();
            System.out.println("-----------------------------------------------------------------------");
            switch (ch) {
                case 1:
                    System.out.println("Enter Product name: ");
                    String pname = sc.next();
                    System.out.println("\nEnter Price: ");
                    int price = sc.nextInt();
                    System.out.println("\nEnter Brand: ");
                    String brand = sc.next();
                    System.out.println("\nEnter Quantity: ");
                    int qty = sc.nextInt();
                    obj.addProduct(++pid, pname, price, brand);
                    listObj.addProductQty(vendorProductList, pid, qty);
                    System.out.println("--------------------------------------------------------------");
                    System.out.println("Product added successfully with PID: " + pid);
                    System.out.println("--------------------------------------------------------------");
                    break;

                case 2:     //Edit a Product in Vendor List
                    listObj.displayProductQty(vendorProductList);
                    System.out.println("\nEnter PID: ");
                    int pid1 = sc.nextInt();
                    if (ProductStore.productList.containsKey(pid1)) {
                        System.out.println("\nEnter Price: ");
                        int price1 = sc.nextInt();
                        System.out.println("\nEnter Brand: ");
                        String brand1 = sc.next();
                        obj.editProduct(pid1, price1, brand1);

                    } else {
                        System.out.println("\nNo such product exists");
                    }
                    break;

                case 3:     //Delete a Product in Vendor List
                    listObj.displayProductQty(vendorProductList);
                    System.out.println("\nEnter PID: ");
                    int pid2 = sc.nextInt();
                    obj.deleteProduct(pid2);
                    vendorProductList.remove(pid2);
                    break;

                case 4:     //Display Products in Vendor List
                    listObj.displayProductQty(vendorProductList);
                    break;

                case 5:
                    System.out.println("\nSearch by name or PID\n\n1. PID\n\n2. Name\n");
                    int choice = sc.nextInt();
                    if(choice == 1){
                        System.out.println("\nEnter PID: ");
                        int pid3 = sc.nextInt();
                        obj.searchProduct(VendorTasks.vendorProductList, pid3);
                    }
                    else{
                        System.out.println("Enter Product name: ");
                        String pname1 = sc.next();
                        obj.searchProduct(VendorTasks.vendorProductList, pname1);
                    }
                    break;

                case 6:
                    flag = 0;
                    break;
            }
        }
    }
}

import java.util.*;

public class ProductListActions{
    Scanner sc = new Scanner(System.in);


    protected void addProductQty(Map<Integer, Integer> list, int pid, int qty){
        if (ProductStore.productList.containsKey(pid)) {
            list.put(pid,qty);

            if(list == Cart.cartList){
                AdminTasks.adminProductList.replace(pid, AdminTasks.adminProductList.get(pid) - qty);
            } else if (list == AdminTasks.adminProductList) {
                VendorTasks.vendorProductList.replace(pid, VendorTasks.vendorProductList.get(pid) - qty);
            }
        } else {
            System.out.println("\nNo such item exists\n");
        }

    }

    protected void displayProductQty(Map<Integer, Integer> list) {
        if (list.isEmpty()) {
            System.out.println("\nNo products in the list");
        } else {
            System.out.println("\nThe products in the list are : ");
            for (Map.Entry<Integer, Integer> me : list.entrySet()) {
                Product pr = (Product) ProductStore.productList.get(me.getKey());
                System.out.println("\nPID : " + me.getKey() + pr + "\nQuantity : " + me.getValue());
                System.out.println();

            }
        }
    }

    public void deleteProductQty(Map<Integer, Integer> list, int pid, int qty) {
        if (list.isEmpty()) System.out.println("\nNo products in the list");
        else {
            int presentQuantity;
                if (list.containsKey(pid)) {
                    System.out.println("\nConfirm to remove the item from list (YES/NO) : ");
                    String ans = sc.next();
                    if (ans.equals("YES") || ans.equals("yes")) {

                        presentQuantity = list.get(pid);

                        if (presentQuantity > qty) {
                            list.replace(pid, presentQuantity - qty);
                            if (list == Cart.cartList) {
                                AdminTasks.adminProductList.replace(pid, AdminTasks.adminProductList.get(pid) + qty);
                            } else if (list == AdminTasks.adminProductList) {
                                VendorTasks.vendorProductList.replace(pid, VendorTasks.vendorProductList.get(pid) + qty);
                            }

                            System.out.println("\nReduced the Quantity from list Successfully");
                        } else {
                            if (list == Cart.cartList) {
                                AdminTasks.adminProductList.replace(pid, AdminTasks.adminProductList.get(pid) + presentQuantity);
                            } else if (list == AdminTasks.adminProductList) {
                                VendorTasks.vendorProductList.replace(pid, VendorTasks.vendorProductList.get(pid) + presentQuantity);
                            }

                            list.remove(pid);
                            System.out.println("\nDeleted from list Successfully");
                        }
                    } else {
                        System.out.println("\nNo item is removed from the list");
                    }
                }else {
                        System.out.println("\nNo such item exists in list\n");
                }

        }

    }
}

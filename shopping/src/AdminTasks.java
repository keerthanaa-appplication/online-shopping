import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class AdminTasks{
    public static Map<Integer, Integer> adminProductList = new HashMap<>();
    ProductListActions obj = new ProductListActions();
    ProductActions pd = new ProductActions();
    public AdminTasks(){
        if(VendorTasks.check){
            pd.addProduct(++VendorTasks.pid, "shoe", 3000, "puma");
            obj.addProductQty(VendorTasks.vendorProductList, VendorTasks.pid, 300);
            pd.addProduct(++VendorTasks.pid, "shirt", 1200, "polo");
            obj.addProductQty(VendorTasks.vendorProductList, VendorTasks.pid, 600);
            pd.addProduct(++VendorTasks.pid, "pant", 1500, "vanHeusen");
            obj.addProductQty(VendorTasks.vendorProductList, VendorTasks.pid, 600);
            pd.addProduct(++VendorTasks.pid, "belt", 500, "black");
            obj.addProductQty(VendorTasks.vendorProductList, VendorTasks.pid, 100);
            pd.addProduct(++VendorTasks.pid, "cap", 70, "black");
            obj.addProductQty(VendorTasks.vendorProductList, VendorTasks.pid, 50);
            pd.addProduct(++VendorTasks.pid, "socks", 300, "nike");
            obj.addProductQty(VendorTasks.vendorProductList, VendorTasks.pid, 200);
            pd.addProduct(++VendorTasks.pid, "wallet", 450, "indigo");
            obj.addProductQty(VendorTasks.vendorProductList, VendorTasks.pid, 100);
            pd.addProduct(++VendorTasks.pid, "salwar", 2000, "trends");
            obj.addProductQty(VendorTasks.vendorProductList, VendorTasks.pid, 500);
            pd.addProduct(++VendorTasks.pid, "saree", 1600, "silk");
            obj.addProductQty(VendorTasks.vendorProductList, VendorTasks.pid, 500);
            pd.addProduct(++VendorTasks.pid, "bag", 2300, "zudio");
            obj.addProductQty(VendorTasks.vendorProductList, VendorTasks.pid, 250);

            VendorTasks.check = false;
        }

        int flag = 1;
        Scanner sc = new Scanner(System.in);
        OfferDetails off = new OfferDetails();
        while(flag == 1)
        {
            System.out.println("-----------------------------------------------------------------------");
            System.out.println("ADMIN TASKS");
            System.out.println();
            System.out.println("choose one\n");
            System.out.println("1. View Products from Vendor\n");
            System.out.println("2. Add a Product\n");
            System.out.println("3. View Products available for Customer\n");
            System.out.println("4. Delete a Product\n");
            System.out.println("5. Search for a Product\n");
            System.out.println("6. Add new offer\n");
            System.out.println("7. Delete an offer\n");
            System.out.println("8. View sold products\n");
            System.out.println("9. Exit from Admin Tasks\n");
            System.out.println();
            System.out.print("enter : ");
            int ch = sc.nextInt();
            System.out.println();
            System.out.println("-----------------------------------------------------------------------");

            switch (ch) {

                case 1:      //View Products from Vendor
                    obj.displayProductQty(VendorTasks.vendorProductList);
                    break;

                case 2 :    //add product

                    obj.displayProductQty(VendorTasks.vendorProductList);
                    System.out.println("\nEnter PID : ");
                    int pid = sc.nextInt();
                    System.out.println("\nEnter Quantity : ");
                    int qty = sc.nextInt();

                    if(qty <= VendorTasks.vendorProductList.get(pid)){
                        obj.addProductQty(adminProductList, pid, qty);
                        System.out.println("--------------------------------------------------------------");
                        System.out.println("Product added successfully with PID: " + pid);
                        System.out.println("--------------------------------------------------------------");
                        obj.displayProductQty(adminProductList);
                    }
                    else{
                        System.out.println("\nRequested Quantity not available ");
                    }
                    break;

                case 3 :    //View Products available online
                    obj.displayProductQty(adminProductList);
                    break;

                case 4 :    //Delete a Product
                    obj.displayProductQty(adminProductList);
                    System.out.println("\nEnter PID: ");
                    int pid2 = sc.nextInt();
                    System.out.println("\nEnter Quantity : ");
                    int qty2 = sc.nextInt();
                    obj.deleteProductQty(adminProductList, pid2, qty2);
                    break;

                case 5 :
                    System.out.println("\nSearch by name or PID\n\n1. PID\n\n2. Name\n");
                    int choice = sc.nextInt();
                    if(choice == 1){
                        System.out.println("\nEnter PID: ");
                        int pid3 = sc.nextInt();
                        pd.searchProduct(VendorTasks.vendorProductList, pid3);
                    }
                    else{
                        System.out.println("Enter Product name: ");
                        String pname1 = sc.next();
                        pd.searchProduct(VendorTasks.vendorProductList, pname1);
                    }
                    break;

                case 6 :
                    off.viewOffer();
                    System.out.println("Enter the amount to be added : ");
                    int amt = sc.nextInt();
                    System.out.println("Enter the discount percentage : ");
                    int dis = sc.nextInt();
                    off.addOffer(amt,dis);
                    break;

                case 7 :
                    off.viewOffer();
                    System.out.println("\nEnter the amount to be deleted : ");
                    int amt1 = sc.nextInt();
                    off.deleteOffer(amt1);
                    break;

                case 8 :    //view sold products
                    SoldProductDetails spd = new SoldProductDetails();
                    spd.viewSoldProducts();
                    break;

                case 9 :
                    flag = 0;
                    break;
            }
        }
    }

}

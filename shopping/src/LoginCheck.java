import java.util.HashMap;

public class LoginCheck {
    public boolean loginUser(HashMap<String, String> userList, String username, String password) {
        if (userList.containsKey(username)) {
            PasswordEncryption obj = new PasswordEncryption();
            String storedPassword = obj.decrypt(userList.get(username));
            //System.out.println("decrypted passwd : " + storedPassword);
            return storedPassword.equals(password);
        } else {
            return false;
        }
    }
}
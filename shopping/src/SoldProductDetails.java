import java.util.*;

public class SoldProductDetails {
    public static Map<Integer , Integer> soldProductList = new HashMap<>();

    public void viewSoldProducts(){
        if(soldProductList.isEmpty()){
            System.out.println("\nNo product in the list");
            return;
        }
        for(Map.Entry<Integer, Integer> me: soldProductList.entrySet()) {
            System.out.println("PID : " + me.getKey() + "\nQuantity sold : " + me.getValue());
            System.out.println();
        }
    }
}

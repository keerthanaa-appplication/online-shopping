public class PersonInfo {
    private String name;
    private String phno;
    PersonInfo(String name, String phno){
        this.name = name;
        this.phno = phno;
    }

    public String getName() {
        return name;
    }

    public String getPhno() {
        return phno;
    }
}

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProductActions{

    Scanner sc = new Scanner(System.in);

    protected void addProduct(int pid, String pname, int price, String brand){
        Product prod = new Product(pname, price, brand);
        ProductStore.productList.put(pid, prod);
    }

    protected void deleteProduct(int pid){
        if (ProductStore.productList.containsKey(pid)){
            System.out.println("Confirm to remove the item from cart (YES/NO) : ");
            String ans = sc.next();
            if(ans.equals("YES") || ans.equals("yes") ){
                ProductStore.productList.remove(pid);
                System.out.println("Deleted Successfully");
            }else{
                System.out.println("No Product is deleted");
            }
        }else{
            System.out.println("No such product exists");
        }
    }

    protected void editProduct(int pid, int price, String brand){
        Product p = (Product) ProductStore.productList.get(pid);
        p.setPrice(price);
        p.setBrand(brand);
        System.out.println("\nEdited details : ");
        System.out.println("\nPID : " + pid + p );
        System.out.println();

    }

    protected void searchProduct(Map<Integer, Integer> list, String pname){
        boolean fl = false;
        for (Map.Entry<Integer, Object> me : ProductStore.productList.entrySet()) {
            if(list.containsKey(me.getKey())){
                Product pr = (Product) me.getValue();
                Pattern pattern = Pattern.compile(pname, Pattern.CASE_INSENSITIVE);
                Matcher matcher = pattern.matcher(pr.getPname());
                boolean matchFound = matcher.find();
                if (matchFound) {
                    System.out.println("\nPID : " + me.getKey() + pr + "\nQuantity : " + list.get(me.getKey()));
                    System.out.println();
                    fl = true;
                }
            }

        }
        if(!fl) System.out.println("\nNo such Product exists");
    }

    protected void searchProduct(Map<Integer, Integer> list, int pid){
        if(ProductStore.productList.containsKey(pid)){
            Product pr = (Product) ProductStore.productList.get(pid);
            System.out.println("\nPID : " + pid + pr + "\nQuantity : " + list.get(pid));
            System.out.println();
        }else{
            System.out.println("\nNo such Product exists");
        }


    }

}

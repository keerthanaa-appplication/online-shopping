public class Product {
    private String pname;
    private int price;
    private String brand;
    private int rating;
    private String review;

    public Product(String pname, int price, String brand){
        this.pname = pname;
        this.price = price;
        this.brand = brand;
        this.review = "";
        this.rating = 0;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }


    public String getPname(){
        return pname;
    }

    public void setPrice(int price){
        this.price = price;
    }
    public int getPrice(){
        return price;
    }

    public void setReview(String review){
        this.review = review;
    }
    public void setRating(int rating){
        this.rating = rating;
    }

    public String getReview(){
        return review;
    }

    public int getRating(){
        return rating;
    }

    @Override
    public String toString() {
        return  "\nProduct name : " + pname + "\nPrice : " + price  +  "\nBrand : " + brand;
    }
}

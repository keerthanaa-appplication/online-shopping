public interface Encryption {
    String encrypt(String plaintext);
    String decrypt(String ciphertext);
}

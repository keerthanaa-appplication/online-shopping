import java.util.*;

public class UserLogin {
    static int cid = 100;
    public static Map<String, String> userAdmin = new HashMap<>();
    public static Map<String, String> userCust = new HashMap<>();
    public static Map<String, String> userVendor = new HashMap<>();
    PasswordEncryption obj = new PasswordEncryption();
    UserLogin(String user, String username, String password) {
        userAdmin.put("adm01", obj.encrypt("aaaaa"));
        userAdmin.put("adm02", obj.encrypt("bbbbb"));

        userCust.put("cust01", obj.encrypt("aaaaa"));
        userCust.put("cust02", obj.encrypt("bbbbb"));

        userVendor.put("ven01", obj.encrypt("aaaaa"));
        userVendor.put("ven02", obj.encrypt("bbbbb"));

        LoginCheck lc = new LoginCheck();
        boolean loggedIn = false;
        if (user.equals("Admin")) {
            loggedIn = lc.loginUser((HashMap<String, String>) userAdmin, username, password);
            if (loggedIn) {
                System.out.println("Logged in Successfully\n");
                AdminTasks ob = new AdminTasks();
            } else {
                System.out.println("Invalid Username or Password\n");
            }
        } else if (user.equals("Vendor")) {
            loggedIn = lc.loginUser((HashMap<String, String>) userVendor, username, password);
            if (loggedIn) {
                System.out.println("Logged in Successfully\n");
                VendorTasks ob = new VendorTasks();
            } else {
                System.out.println("Invalid Username or Password\n");
            }
        } else {
            loggedIn = lc.loginUser((HashMap<String, String>) userCust,username, password);
            if (loggedIn) {
                System.out.println("Logged in Successfully\n");
                cid++;
                Cart ct = new Cart();
                CustomerTasks ob = new CustomerTasks();
            } else {
                System.out.println("Invalid Username or Password\n");
            }
        }
    }

}

